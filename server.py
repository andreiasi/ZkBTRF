from flask import Flask,Response,render_template,request, session, redirect, url_for
import json


app=Flask(__name__)

@app.route("/")
@app.route("/home")
def home():
    if not session.get('username'):
        return render_template('home.html')
    else:
        return render_template('home.html',message="welcome "+session.get('username'))

@app.route("/about")
def about():
    return render_template("about.html")   

@app.route("/mylife")
def mylife():
    return render_template("mylife.html")

@app.route("/history")
def education():
    return render_template("history.html")

@app.route("/contact")
def contact():
    return render_template("contact.html")

@app.route("/content-api", methods=['GET'])
def contentApi():
    _content_path = request.args.get('source_field')
    return render_template(_content_path)


@app.route("/login",methods=['POST','GET'])
def login():
    if request.method.lower() == 'get':
        return render_template("login.html")
    elif request.method.lower() == 'post':

        _user=request.form['username']
        _pass=request.form['password']

        file_data=open('user.json','r').read()
        user_data=json.loads(file_data)




        for item in user_data:
            if item['username']==_user and item['password']==_pass:
                session['username']=_user
                break

        
        if session.get('username'):
            return Response(url_for('home'), status=200)
        else :
            return Response("user/pass combination not right!", status=400)

        #if request.form['username'] == _user and request.form['password'] == _pass:
        #   session['username'] = _user
        #   return redirect(url_for('home'))
        #else:
        #   return render_template('login.html',message="user/pass combination not right!")

@app.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('home'))    

@app.route("/register",methods=['post','get'])
def register():
    if request.method.lower()=='get':
        return render_template('register.html')
    elif request.method.lower()=='post':

        _username=request.form['username']
        _password1=request.form['password1']
        # _password2=request.form['password2']

        file_data=open('user.json','r').read()
        users_data=json.loads(file_data)

        if len(_username)<4:
            return Response("username should have more than 4 characters!", status=200)
        # elif _password1 != _password2:
        #     return render_template('register.html',message="passwords don't match")
        for item in users_data:
            if item['username']==_username:
                return Response("username exitst! try another one", status=200)
        users_data.append(
            {
                "username":_username,
                "password":_password1

            }
        )

        out_file=open('user.json','w')
        json.dump(users_data,out_file,indent=4)

        return Response('user registered. Click <a href="/login">here</a> to login', status=200)




if __name__=="__main__":
    app.secret_key = "287"
    app.run(debug=True)

 
