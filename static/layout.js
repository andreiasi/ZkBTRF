$(document).ready(function () {

    $("#alert-close").click(function () {
        _hideAlert();
    });

    function _hideAlert() {
        $(".alert-container").addClass('d-none');
        $(".alert-content").text('');
    }


    function _showAlert(msg) {
        $(".alert-container").removeClass('d-none');
        $(".alert-content").text(msg);
    }

});
