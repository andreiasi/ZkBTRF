$(document).ready(function () {

    $("#login").click(function () {
        _submitLoginForm();
    });

    function _submitLoginForm() {
        var username = $("#username").val();
        var password = $("#password").val();

        if (!username.length)
            _showAlert("Please fill the username");
        else if (!password.length)
            _showAlert("Please fill the password");
        else {
            var data = {
                username: username,
                password: password
            };

            console.log(data);
            $.ajax({
                type: "POST",
                url: "/login",
                data: data,
                dataType: 'json'
            }).always(function (xhr) {
                _response(xhr);
            });
        }
    }

    function _response(resp) {
        if(resp.status === 400)
            _showAlert(resp.responseText);
        else
            window.location = resp.responseText;
    }

    function _showAlert(msg) {
        $(".alert-container").removeClass('d-none');
        $(".alert-content").text(msg);
    }
});
