$(document).ready(function () {

    $(".get-content").click(function () {
        var source = $(this).data('content');
        _requestContent(source);
    });

    function _requestContent(source) {

        var data = {source_field: source};
        console.log(data);
        $.ajax({
            type: "GET",
            url: "/content-api",
            data: data,
            dataType: 'json'
        }).always(function (xhr) {
            _response(xhr);
        });

    }

    function _response(resp) {
        if (resp.status === 400)
            _showAlert(resp.responseText);
        else
            $(".load-container").html(resp.responseText);
    }

    function _showAlert(msg) {
        $(".alert-container").removeClass('d-none');
        $(".alert-content").text(msg);
    }
});
