$(document).ready(function () {

    $("#register").click(function () {
        _submitRegisterForm();
    });

    function _submitRegisterForm() {
        var username = $("#username").val();
        var password1 = $("#password1").val();
        var confirm_password = $("#confirm-password").val();

        if (!username.length)
            _showAlert("Please fill the username");
        else if (!password1.length)
            _showAlert("Please fill the password");
        else if (password1 !== confirm_password)
            _showAlert("The confirm password doesn't match");
        else {
            var data = {
                username: username,
                password1: password1
            };

            console.log(data);
            $.ajax({
                type: "POST",
                url: "/register",
                data: data,
                dataType: 'json'
            }).always(function (xhr) {
                _response(xhr);
            });
        }
    }

    function _response(resp) {
        _showAlert(resp.responseText);
    }

    function _showAlert(msg) {
        $(".alert-container").removeClass('d-none');
        $(".alert-content").html(msg);
    }
});
